# **Election Analysis Starter Kit**

## **Disclaimer**

## This repository is intended for educational, non-commercial, and research purposes only. Some of the data pulled using the various workflows has restrictions on commercial purposes. This repository does not, and is not intended to, constitute legal advice, and users of this repository should contact their attorney to obtain legal advice with respect to any particular legal matter.

##  **How to run**

## 1. Install docker: [link here](https://docs.docker.com/install/#support)

## 2. Install docker-compose: [link here](https://docs.docker.com/compose/install/)

## 3. (optional for gpu) Install nvidia-docker and update your docker driver: [link here](https://github.com/NVIDIA/nvidia-docker)

## 4. Generate the env files using the click script:
## <code>python create_config.py</code>

## 5. Build the necessary docker images:
## <code>docker-compose build</code>

## 6. Run docker-compose:
## <code>docker-compose up -d</code>

# The next steps use dagster to run the data ingestion. You can find more about how to run pipelines in dagster [here](https://dagster.readthedocs.io/en/0.6.7.post0/sections/learn/tutorial/hello_cereal.html#executing-our-first-pipeline) 

## 7. Load the election data by running the "add_election_results_pipeline" on dagster (port 3000)

## 8. Load the campaign finance data by running the "add_campaign_finance_pipeline" on dagster (port 3000)

## 9. Load the voter file data by running the "rebuild_voter_file_pipline" on dagster (port 3000)

## 10. go into the voter_file_api container "docker exec -it eask_voter_file_api" and run "vf_api db upgrade" to create the necessary tables for the api

## **Note**
## If any of the database operations fail, check to make sure your postgres container has run the creation scripts.
## If not, "docker exec -it eask_postgres" then cd into "docker-entrypoint-initdb.d" and run the scripts by "bash <>" for each script


## Service - Port Mappings
| Service | Port |
| --- | --- |
| Dagster/Dagit | 3000 |
| Jupyter | 8888 |
| Minio | 9000 |
| Portainer | 9090 |
| Postgres | 5435 |
| Redis | 6379 |
| Voter File API | 7070 |

## **Coming Soon**

## 1. Dagstermill pipelines to update election analysis notebooks

## 2. Adding pipeline config presets in docker to make it easier to build the pipeline configs

## **Note**

## Docker on Windows has some strange permission behavior for the shared directories. You may need to copy them manually using volumes.

***
Original template via [data-science-stack](https://github.com/jgoerner/data-science-stack-cookiecutter) 
