import random
import string

import click

api_database = """
API_PASSWORD={}
"""

jupyter = """
PASSWORD={}
"""

minio = """
MINIO_ACCESS_KEY={}
MINIO_SECRET_KEY={}
"""

shared_database = """
SHARED_PASSWORD={}
"""

postgres = """
POSTGRES_PASSWORD={}
SHARED_PASSWORD={}
"""

api_database = """
API_PASSWORD={}
"""
vf_api = """
VF_API_SECRET_KEY="{}"
VF_API_DATABASE_URI=postgres://api:{}@postgres/api
VF_API_VOTERFILE_DATABASE_URI=postgres://shared:{}@postgres/shared
VF_API_RQ_REDIS_URL=redis://redis:6379/0
"""

confirm_string = """
jupyter password: {}
minio access: {}
minio secret: {}
postgres db password: {}
shared db password: {}
api db password: {}
api secret key: {}
"""


def generate_random_string(length):
    letters = string.ascii_lowercase + string.ascii_uppercase
    return ''.join(random.choice(letters) for i in range(length))


def write_env_file(env_loc, env_string):
    with open(env_loc, 'w') as f:
        #Need to remoe first new line character
        f.write(env_string[1:])


@click.command()
def create_config():
    while True:
        jupyter_password = click.prompt(
            "Enter jupyter password", 
            default = generate_random_string(20),
            type = str
        )
        minio_access = click.prompt(
            "Enter minio access key",
            default = generate_random_string(20), 
            type = str
        )
        minio_secret = click.prompt(
            "Enter minio secret key",
            default = generate_random_string(20), 
            type = str
        )
        postgres_db_password = click.prompt(
            "Enter postgres db password",
            default = generate_random_string(20),
            type = str
        )
        shared_db_password = click.prompt(
            "Enter shared db password",
            default = generate_random_string(20),
            type = str
        )
        api_db_password = click.prompt(
            "Enter api db password",
            default = generate_random_string(20),
            type = str
        )
        vf_api_secret_key = click.prompt(
            "Enter api secret key",
            default = generate_random_string(20),
            type = str
        )
        print(confirm_string.format(
            jupyter_password,
            minio_access,
            minio_secret,
            postgres_db_password,
            shared_db_password,
            api_db_password,
            vf_api_secret_key
        ))
        if click.confirm('Is this correct?'):
            break
    write_env_file(
        './config/jupyter.env',
        jupyter.format(jupyter_password)
    )
    write_env_file(
        './config/minio.env',
        minio.format(minio_access, minio_secret)
    )
    write_env_file(
        './config/postgres.env',
        postgres.format(postgres_db_password, shared_db_password)
    )
    write_env_file(
        './config/shared_database.env',
        shared_database.format(shared_db_password)
    )
    write_env_file(
        './config/api_database.env',
        api_database.format(api_db_password)
    )
    write_env_file(
        './config/vf_api.env',
        vf_api.format(
            vf_api_secret_key,
            api_db_password,
            shared_db_password
        )
    )
        
        
if __name__ == "__main__":
    create_config()
