from dagster import RepositoryDefinition

from pipelines.campaign_finance_ingestion import (
    add_campaign_finance_pipeline,
    rebuild_campaign_finance_pipeline,
)
from pipelines.election_results_ingestion import (
    add_election_results_pipeline,
    rebuild_election_results_pipeline,
)
from pipelines.voter_file_ingestion import (
    rebuild_voter_file_pipeline,
    rebuild_voter_file_lookups_pipeline,
)

def define_repo():
    return RepositoryDefinition(
        name='campaign_data_repository',
        # Note that we can pass a function, rather than pipeline instance.
        # This allows us to construct pipelines lazily, if, e.g.,
        # initializing a pipeline involves any heavy compute
        pipeline_dict={
            'add_campaign_finance_pipeline': lambda: add_campaign_finance_pipeline,
            'rebuild_campaign_finance_pipeline': lambda: rebuild_campaign_finance_pipeline,
            'add_election_results_pipeline': lambda: add_election_results_pipeline,
            'rebuild_election_results_pipeline': lambda: rebuild_election_results_pipeline,
            'rebuild_voter_file_pipeline': lambda: rebuild_voter_file_pipeline,
            'rebuild_voter_file_lookups_pipeline': lambda: rebuild_voter_file_lookups_pipeline,
        },
    )
