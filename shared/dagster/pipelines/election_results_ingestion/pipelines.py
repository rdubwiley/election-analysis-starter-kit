from dagster import pipeline

from solids.data_collection.election_results import (
    drop_tables,
    check_years,
    save_election_data,
    upload_election_data,
    cleanup_data,
)
from solids.dummy import return_true

@pipeline
def add_election_results_pipeline():
    check = return_true()
    year_dict = check_years(check=check)
    files = save_election_data(year_dict)
    files_to_remove = upload_election_data(files)
    cleanup_data(files_to_remove)

@pipeline
def rebuild_election_results_pipeline():
    check = drop_tables()
    year_dict = check_years(check=check)
    files = save_election_data(year_dict)
    files_to_remove = upload_election_data(files)
    cleanup_data(files_to_remove)
