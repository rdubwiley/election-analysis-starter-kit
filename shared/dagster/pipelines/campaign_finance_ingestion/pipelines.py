from dagster import pipeline

from solids.data_collection.campaign_finance import (
    delete_campaign_finance_tables,
    construct_campaign_finance_dict,
    save_campaign_finance_data,
    upload_campaign_finance_data,
    cleanup_finance_data,
)
from solids.dummy import return_true

@pipeline
def add_campaign_finance_pipeline():
    check = return_true()
    year_dict = construct_campaign_finance_dict(check=check)
    files = save_campaign_finance_data(year_dict)
    files_to_remove = upload_campaign_finance_data(files)
    cleanup_finance_data(files_to_remove)

@pipeline
def rebuild_campaign_finance_pipeline():
    check = delete_campaign_finance_tables()
    year_dict = construct_campaign_finance_dict(check=check)
    files = save_campaign_finance_data(year_dict)
    files_to_remove = upload_campaign_finance_data(files)
    cleanup_finance_data(files_to_remove)