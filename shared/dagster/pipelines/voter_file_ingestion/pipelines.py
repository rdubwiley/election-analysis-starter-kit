from dagster import pipeline

from solids.data_collection.voter_file import (
    check_voter_file,
    rebuild_lookups,
    upload_voter_file_data,
    cleanup_voter_file_data,
)

@pipeline
def rebuild_voter_file_pipeline():
    file_list = check_voter_file()
    to_delete = upload_voter_file_data(file_list)
    cleanup_voter_file_data(to_delete)

@pipeline
def rebuild_voter_file_lookups_pipeline():
    file_list = rebuild_lookups()
    to_delete = upload_voter_file_data(file_list)
    cleanup_voter_file_data(to_delete)