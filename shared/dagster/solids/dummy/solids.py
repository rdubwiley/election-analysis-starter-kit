from dagster import solid, Bool

@solid()
def return_true(context) -> Bool:
    return True