from io import BytesIO
import zipfile
from zipfile import ZipFile

from dagster import (
    composite_solid,
    solid, 
    Field, 
    Bool,
    String,
    Optional,
    Dict,
    List,
    as_dagster_type,
    Output,
    OutputDefinition,
    Materialization,
    EventMetadataEntry,
    JsonMetadataEntryData
)
import requests

def get_zip_file(url:str) -> ZipFile:
    #Generic function which we can import into our other solids
    r = requests.get(url)
    return zipfile.ZipFile(BytesIO(r.content))

@solid()
def zip_file_web_solid(context, url:str) -> ZipFile:
    return get_zip_file(url)
