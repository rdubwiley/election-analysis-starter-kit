from io import StringIO
import os

from numpy import dtype
import pandas as pd
from requests_html import HTMLSession
from sqlalchemy import create_engine

from .read_schemas import (
    county_schema,
    elections_schema,
    juris_schema,
    school_schema,
    village_schema
)

voter_file_tables = [
    'voters',
    'voter_history'
]

voter_dtypes = {
    'LAST_NAME': dtype('O'), 
    'FIRST_NAME': dtype('O'), 
    'MIDDLE_NAME': dtype('O'), 
    'NAME_SUFFIX': dtype('O'), 
    'YEAR_OF_BIRTH': dtype('int64'), 
    'GENDER': dtype('O'), 
    'REGISTRATION_DATE': dtype('O'), 
    'STREET_NUMBER_PREFIX': dtype('O'), 
    'STREET_NUMBER': dtype('O'), 
    'STREET_NUMBER_SUFFIX': dtype('O'), 
    'DIRECTION_PREFIX': dtype('O'), 
    'STREET_NAME': dtype('O'), 
    'STREET_TYPE': dtype('O'), 
    'DIRECTION_SUFFIX': dtype('O'), 
    'EXTENSION': dtype('O'), 
    'CITY': dtype('O'), 
    'STATE': dtype('O'), 
    'ZIP_CODE': dtype('O'), 
    'MAILING_ADDRESS_LINE_ONE': dtype('O'), 
    'MAILING_ADDRESS_LINE_TWO': dtype('O'), 
    'MAILING_ADDRESS_LINE_THREE': dtype('O'), 
    'MAILING_ADDRESS_LINE_FOUR': dtype('O'), 
    'MAILING_ADDRESS_LINE_FIVE': dtype('O'), 
    'VOTER_IDENTIFICATION_NUMBER': dtype('int64'), 
    'COUNTY_CODE': dtype('int64'), 
    'COUNTY_NAME': dtype('O'), 
    'JURISDICTION_CODE': dtype('int64'), 
    'JURISDICTION_NAME': dtype('O'), 
    'PRECINCT': dtype('O'), 
    'WARD': dtype('O'), 
    'SCHOOL_DISTRICT_CODE': dtype('int64'), 
    'SCHOOL_DISTRICT_NAME': dtype('O'), 
    'STATE_HOUSE_DISTRICT_CODE': dtype('int64'), 
    'STATE_HOUSE_DISTRICT_NAME': dtype('O'), 
    'STATE_SENATE_DISTRICT_CODE': dtype('int64'), 
    'STATE_SENATE_DISTRICT_NAME': dtype('O'), 
    'US_CONGRESS_DISTRICT_CODE': dtype('int64'), 
    'US_CONGRESS_DISTRICT_NAME': dtype('O'), 
    'COUNTY_COMMISSIONER_DISTRICT_CODE': dtype('int64'), 
    'COUNTY_COMMISSIONER_DISTRICT_NAME': dtype('O'), 
    'VILLAGE_DISTRICT_CODE': dtype('O'), 
    'VILLAGE_DISTRICT_NAME': dtype('O'), 
    'VILLAGE_PRECINCT': dtype('O'), 
    'SCHOOL_PRECINCT': dtype('O'), 
    'IS_PERMANENT_ABSENTEE_VOTER': dtype('O'), 
    'VOTER_STATUS_TYPE_CODE': dtype('O'),
    'UOCAVA_STATUS_CODE': dtype('O'), 
    'UOCAVA_STATUS_NAME': dtype('O')
}

voter_history_dtypes = {

}

def check_for_tables(engine):
    missing_tables = []
    for table in voter_file_tables:
        if not engine.dialect.has_table(engine, table):
            missing_tables.append(table)
    return missing_tables

def get_latest_voter_file_url():
    session = HTMLSession()
    base_url = 'http://michiganvoters.info/download/'
    r = session.get(base_url)
    all_links = []
    for link in r.html.links:
        if link[0] == '2':
            all_links.append(link)
    return base_url+max(all_links)

def create_schema():
    pwd = os.environ["SHARED_PASSWORD"]
    con = create_engine("postgres://shared:{}@postgres/shared".format(pwd))
    con.execute('CREATE SCHEMA IF NOT EXISTS voter_file;')
    
def load_csv(csv_loc, table, schema='voter_file'):
    """
    Load a csv overwriting the underlying table if it exists and then chunking through the file
    """
    pwd = os.environ["SHARED_PASSWORD"]
    engine = create_engine(
        "postgres://shared:{pwd}@postgres/shared".format(**locals()))
    data_dtypes = None
    if table == 'voters':
        data_dtypes = voter_dtypes
    if data_dtypes:
        chunks = pd.read_csv(csv_loc, sep="|", chunksize=500000, dtype=data_dtypes)
    else:
        chunks = pd.read_csv(csv_loc, sep="|", chunksize=500000)
    for i, chunk in enumerate(chunks):
        if i == 0:
            chunk.to_sql(table, engine, if_exists='replace', index=False, schema=schema)
        else:
            chunk.to_sql(table, engine, if_exists='append', index=False, schema=schema)

def transform_data(schema, input_loc, output_loc):
    chunks = pd.read_fwf(input_loc,
                        colspecs=[ (r[1]-1, r[1]+r[2]-1) for r in schema ],
                        names=[ r[0] for r in schema ],
                        chunksize = 50000
                      )
    for i, chunk in enumerate(chunks):
        if i == 0:
            chunk.to_csv(output_loc, index=False, sep="|")
        else:
            with open(output_loc, 'a') as f:
                chunk.to_csv(f, header=False, index=False, sep="|")

def get_lst_file(voter_file_url, table):
    session = HTMLSession()
    file_url = voter_file_url + f"{table}cd.lst"
    r = session.get(file_url)
    return StringIO(r.text)

def read_lookup_files(voter_file_url, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    read_tuples = [
        (county_schema, get_lst_file(voter_file_url, 'county'), output_dir+'counties.csv', 'counties'),
        (elections_schema, get_lst_file(voter_file_url, 'elections'), output_dir+'elections.csv', 'elections'),
        (juris_schema, get_lst_file(voter_file_url, 'jurisd'), output_dir+'jurisdictions.csv', 'jurisdictions'),
        (school_schema, get_lst_file(voter_file_url, 'school'), output_dir+'schools.csv', 'schools'),
        (village_schema, get_lst_file(voter_file_url, 'village'), output_dir+'villages.csv', 'villages'),
    ]
    for t in read_tuples:
        transform_data(t[0], t[1], t[2])
    return [ {'location': t[2], 'table': t[3]} for t in read_tuples]

def update_lookup_file_list(lookup_file_list, table, location):
    for i, item in enumerate(lookup_file_list):
        if item['table'] == table:
            lookup_file_list[i]['location'] = location
    return lookup_file_list
