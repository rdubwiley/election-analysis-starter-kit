voter_schema = [
                #(column_name, start position, field length)
                ('last_name',1,35),
                ('first_name',36,20),
                ('middle_name',56,20),
                ('name_suffix',76,3),
                ('birth_year',79,4),
                ('gender',83,1),
                ('date_of_registration',84,8),
                ('house_number_character',92,1),
                ('residence_street_number',93,7),
                ('house_suffix',100,4),
                ('pre-direction',104,2),
                ('street_name',106,30),
                ('street_type',136,6),
                ('suffix_direction',142,2),
                ('residence_extension',144,13),
                ('city',157,35),
                ('state',192,2),
                ('zip',194,5),
                ('mail_address_1',199,50),
                ('mail_address_2',249,50),
                ('mail_address_3',299,50),
                ('mail_address_4',349,50),
                ('mail_address_5',399,50),
                ('voter_id',449,13),
                ('county_code',462,2),
                ('jurisdiction',464,5),
                ('ward_precinct',469,6),
                ('school_code',475,5),
                ('state_house',480,5),
                ('state_senate',485,5),
                ('us_congress',490,5),
                ('county_commissioner',495,5),
                ('village_code',500,5),
                ('village_precinct',505,6),
                ('school_precinct',511,6),
                ('permanent_absentee_ind',517,1),
                ('status_type',518,2),
                ('uocava_status',520,1)
]

history_schema = [
                    ('voter_id',1,13),
                    ('county_code',14,2),
                    ('jurisdiction',16,5),
                    ('school_code',21,5),
                    ('election_code',26,13),
                    ('absentee_voter_indicator',39,1),
]

county_schema = [
                    ('county_code',1,2),
                    ('county_name',3,25)
]

elections_schema = [
                    ('election_code',1,13),
                    ('election_month', 14, 2),
                    ('election_day', 16, 2),
                    ('election_year', 18, 4),
                    ('election_name',22,30)
]

juris_schema = [
                    ('county_code',1,2),
                    ('jurisdiction_code',3,5),
                    ('jurisdiction_name',8,35)
]

school_schema = [
                    ('county_code',1,2),
                    ('jurisdiction_code',3,5),
                    ('schooldist_code',8,5),
                    ('schooldist_name',13,50)
]

village_schema = [
                    ('village_id',1,13),
                    ('county_code',14,2),
                    ('jurisdiction_code',16,5),
                    ('village_dist_code',21,5),
                    ('village_name',26,50)
]