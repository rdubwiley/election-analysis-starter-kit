import os

from dagster import (
    composite_solid,
    solid, 
    Field, 
    Bool,
    String,
    Optional,
    Dict,
    List,
    as_dagster_type,
    Output,
    OutputDefinition,
    Materialization,
    EventMetadataEntry,
    JsonMetadataEntryData
)
import pandas as pd

from .utils import (
    get_latest_voter_file_url,
    read_lookup_files,
    update_lookup_file_list,
    create_schema,
    load_csv,
)
from ..web_requests import get_zip_file

@solid()
def check_voter_file(
    context, 
    voter_loc:Optional[String]=None, 
    voter_history_loc:Optional[String]=None,
    county_loc:Optional[String]=None,
    elections_loc:Optional[String]=None,
    juris_loc:Optional[String]=None,
    school_loc:Optional[String]=None,
    village_loc:Optional[String]=None,
) -> List[Dict]:
    file_list = []
    voter_file_url = get_latest_voter_file_url()
    if not voter_loc:
        voter_url = voter_file_url+'EntireStateVoter.zip'
        voter_zip = get_zip_file(voter_url)
        file_name = voter_zip.namelist()[0]
        df = pd.read_csv(
            voter_zip.open(file_name), 
            error_bad_lines=False, 
            encoding='iso-8859-1',
            low_memory = False, 
            warn_bad_lines=False
        )
        file_out = "./data/voter_file/voters.csv"
        df.to_csv(file_out, index=False, sep="|")
        file_list.append({'table': 'voters', 'location':file_out})
    else:
        file_list.append({'table': 'voters', 'location':voter_loc})
    if not voter_history_loc:
        voter_history_url = voter_file_url+'EntireStateVoterHistory.zip'
        voter_history_zip = get_zip_file(voter_history_url)
        file_name = voter_history_zip.namelist()[0]
        df = pd.read_csv(
            voter_history_zip.open(file_name), 
            error_bad_lines=False, 
            encoding='iso-8859-1',
            low_memory = False, 
            warn_bad_lines=False
        )
        file_out = "./data/voter_file/voter_history.csv"
        df.to_csv(file_out, index=False, sep="|")
        file_list.append({'table': 'voter_history', 'location':file_out})
    else:
        file_list.append({'table': 'voter_history', 'location':voter_history_loc})
    lookup_file_list = read_lookup_files(voter_file_url, './data/voter_file/lookups/')
    if county_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'counties', county_loc)
    if elections_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'elections', elections_loc)
    if juris_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'juris', juris_loc)
    if school_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'schools', school_loc)
    if village_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'villages', village_loc)
    for item in lookup_file_list:
        file_list.append(item)
    return file_list

@solid()
def rebuild_lookups(
    context,
    county_loc:Optional[String]=None,
    elections_loc:Optional[String]=None,
    juris_loc:Optional[String]=None,
    school_loc:Optional[String]=None,
    village_loc:Optional[String]=None,
) -> List[Dict]:
    file_list = []
    voter_file_url = get_latest_voter_file_url()
    lookup_file_list = read_lookup_files(voter_file_url, './data/voter_file/lookups/')
    if county_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'counties', county_loc)
    if elections_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'elections', elections_loc)
    if juris_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'juris', juris_loc)
    if school_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'schools', school_loc)
    if village_loc:
        lookup_file_list = update_lookup_file_list(lookup_file_list, 'villages', village_loc)
    for item in lookup_file_list:
        file_list.append(item)
    return file_list

@solid()
def upload_voter_file_data(context, file_list:List[Dict]) -> List[Dict]:
    create_schema()
    for data in file_list:
        load_csv(data['location'], data['table'])
        context.log.info(f"Loaded {data['location']}")
    return file_list

@solid()
def cleanup_voter_file_data(context, file_list:List[Dict]):
    for data in file_list:
        context.log.info(f"Removed {data['location']}")
        os.remove(data['location'])