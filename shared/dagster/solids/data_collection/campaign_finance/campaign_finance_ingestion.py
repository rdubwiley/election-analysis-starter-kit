import os
from zipfile import ZipFile

from dagster import (
    composite_solid,
    solid, 
    Field, 
    Bool,
    String,
    Optional,
    Dict,
    List,
    as_dagster_type,
    Output,
    OutputDefinition,
    Materialization,
    EventMetadataEntry,
    JsonMetadataEntryData
)

from .utils import (
    campaign_finance_tables,
    delete_table_by_years,
    check_campaign_finance_data,
    read_campaign_finance_year,
    save_campaign_finance_year,
    load_campaign_finance_csv,
)

@solid()
def delete_campaign_finance_tables(context, table_dict):
    """
    Solid to delete a given year of data from a given table

    data should be in the form

    table:
      -year1
      -year2
    """
    for table in table_dict:
        if table in campaign_finance_tables:
            delete_table_by_years(table, table_dict[table])
            context.log.info(f"For table {table} deleted years {table_dict[table]}")
        else:
            context.log.error(f"Table {table} not in campaign finance tables")
    return True

@solid()
def construct_campaign_finance_dict(context, years:List[String], check:Optional[Bool]=None) -> Dict:
    return check_campaign_finance_data(years)

@solid()
def save_campaign_finance_data(context, year_dict) -> List[Dict]:
    output_files = []
    for year in year_dict:
        if year_dict[year]:
            data = read_campaign_finance_year(year)
            for table in year_dict[year]:
                filename = f'./data/campaign_finance/{table}_{year}.csv'
                data[table].to_csv(filename, index=False, sep="|")
                output_files.append({
                    'location': filename,
                    'table': table
                })
    return output_files

@solid()
def upload_campaign_finance_data(context, data_files:List[Dict]) -> List[Dict]:
    if not os.path.isdir("data"):
        os.makedirs("./data")
    if not os.path.isdir("data/campaign_finance"):
        os.makedirs("./data/campaign_finance")
    for item in data_files:
        load_campaign_finance_csv(item['location'], item['table'])
        context.log.info(f"Loaded file {item['location']}")
    return data_files

@solid()
def cleanup_finance_data(context, file_list:List[Dict]):
    for data in file_list:
        context.log.info(f"Removed {data['location']}")
        os.remove(data['location'])