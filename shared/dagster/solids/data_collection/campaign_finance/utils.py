import os
from io import BytesIO
from zipfile import ZipFile
from urllib.request import urlopen

import pandas as pd
from sqlalchemy import create_engine
from requests_html import HTMLSession


campaign_finance_tables = [
    'expenditures',
    'receipts',
    'contributions'
]

def delete_table_by_years(table, years):
    pwd = os.environ["SHARED_PASSWORD"]
    engine = create_engine(
        "postgres://shared:{pwd}@postgres/shared".format(**locals()))
    raw_query = "DELETE from {} WHERE doc_stmnt_year='{}'"
    for year in years:
        query = raw_query.format(table, year)
        engine.execute(query)
        
def check_for_tables(engine):
    missing_tables = []
    for table in campaign_finance_tables:
        if not engine.dialect.has_table(engine, table):
            missing_tables.append(table)
    return missing_tables

def missing_table_years(engine, table, years):
    query = "SELECT doc_stmnt_year from {} GROUP BY doc_stmnt_year".format(table)
    df = pd.read_sql(query, engine)
    table_years = list(df['doc_stmnt_year'].unique())
    table_years = [ str(year) for year in table_years ]
    missing_years = []
    for year in years:
        if year not in table_years:
            missing_years.append(year)
    return missing_years

def check_campaign_finance_data(years):
    pwd = os.environ["SHARED_PASSWORD"]
    engine = create_engine(
        "postgres://shared:{pwd}@postgres/shared".format(**locals()))
    needed_files = { year: [] for year in years }
    missing_tables = check_for_tables(engine)
    for year in years:
        for table in missing_tables:
            needed_files[year].append(table)
    for table in campaign_finance_tables:
        if table not in missing_tables:
            missing_years = missing_table_years(engine, table, years)
            for year in missing_years:
                needed_files[year].append(table)
    return needed_files

def clean_files(raw_dir, year_dict):
    max_year = max([ year for year in year_dict])
    files = os.listdir(raw_dir)
    for file in files:
        try:
            table = file.split("_")[0]
            year = int(file.split("_")[1].split(".")[0])
        except Exception:
            continue
        if table in campaign_finance_tables and table not in year_dict[year]:
            os.remove("{}/{}".format(raw_dir, file))
        elif table not in campaign_finance_tables:
            if year != max_year:
                os.remove("{}/{}".format(raw_dir, file))

contribution_columns = [
    'doc_seq_no',
    'page_no',
    'contribution_id',
    'cont_detail_id',
    'doc_stmnt_year',
    'doc_type_desc',
    'com_legal_name',
    'common_name',
    'cfr_com_id',
    'com_type',
    'can_first_name',
    'can_last_name',
    'contribtype',
    'f_name',
    'l_name_or_org',
    'address',
    'city',
    'state',
    'zip',
    'occupation',
    'employer',
    'received_date',
    'amount',
    'aggregate',
    'extra_desc'
]

expenditure_columns = [
    'doc_seq_no', 
    'expenditure_type', 
    'gub_account_type', 
    'gub_elec_type', 
    'page_no', 
    'expense_id', 
    'detail_id', 
    'doc_stmnt_year', 
    'doc_type_desc', 
    'com_legal_name', 
    'common_name', 
    'cfr_com_id', 
    'com_type', 
    'schedule_desc', 
    'exp_desc', 
    'purpose', 
    'extra_desc', 
    'f_name', 
    'lname_or_org', 
    'address', 
    'city', 
    'state', 
    'zip', 
    'exp_date', 
    'amount', 
    'state_loc', 
    'supp_opp', 
    'can_or_ballot', 
    'county', 
    'debt_payment', 
    'vend_name', 
    'vend_addr', 
    'vend_city', 
    'vend_state', 
    'vend_zip', 
    'gotv_ink_ind', 
    'fundraiser'
]    

receipt_columns = [
    'doc_seq_no', 
    'ik_code', 
    'gub_account_type', 
    'gub_elec_type', 
    'page_no', 
    'receipt_id', 
    'detail_id', 
    'doc_stmnt_year', 
    'doc_type_desc', 
    'com_legal_name', 
    'common_name', 
    'cfr_com_id', 
    'com_type', 
    'can_first_name', 
    'can_last_name', 
    'contribtype', 
    'f_name', 
    'l_name_or_org', 
    'address', 
    'city', 
    'state', 
    'zip', 
    'occupation', 
    'employer', 
    'received_date', 
    'amount', 
    'aggregate', 
    'extra_desc', 
    'receipttype'
]

def read_url(url, sep="\t", header=True):
    resp = urlopen(url)
    zipfile = ZipFile(BytesIO(resp.read()))
    output = []
    for name in zipfile.namelist():
        if header is None:
            df = pd.read_csv(
                zipfile.open(name), 
                sep=sep, 
                error_bad_lines=False, 
                encoding='iso-8859-1', 
                header=header, 
                low_memory = False, 
                warn_bad_lines=False
            )
        else:
            df = pd.read_csv(
                zipfile.open(name), 
                sep=sep, 
                error_bad_lines=False, 
                encoding='iso-8859-1', 
                low_memory=False, 
                warn_bad_lines=False)
        output.append(df)
    return output

def read_campaign_finance_year(year):
    session = HTMLSession()
    base_url = 'http://miboecfr.nictusa.com/cfr/dumpall/cfrdetail/'
    r = session.get(base_url)
    output = {
        'contributions': [],
        'expenditures': [],
        'receipts': []
    }
    for link in r.html.links:
        if str(year) not in link:
            continue
        try:
            if 'expenditures' in link:
                df = read_url(base_url+link)[0]
                df = df.loc[:, expenditure_columns]
                output['expenditures'].append(df)
            elif 'contributions' in link:
                if link.split("_")[-1] != "00.zip" and link.split("_")[-1][0] == "0":
                    df = read_url(base_url+link, header=None)[0]
                    df = df.loc[:, range(25)]
                    df = df.reindex(columns = contribution_columns)
                    df = df.loc[:, contribution_columns]
                else:
                    df = read_url(base_url+link)[0]
                    df = df.loc[:, contribution_columns]
                output['contributions'].append(df)
            elif 'receipts' in link:
                df = read_url(base_url+link)[0]
                df = df.loc[:, receipt_columns]
                output['receipts'].append(df)
        except Exception as exc:
            print(f"error with {link}")
    output['contributions'] = pd.concat(output['contributions'], join="inner").reset_index(drop=True)
    output['receipts'] = pd.concat(output['receipts'], join="inner").reset_index(drop=True)
    output['expenditures'] = pd.concat(output['expenditures'], join="inner").reset_index(drop=True)
    return output

def save_campaign_finance_year(year, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    output = read_campaign_finance_year(year)
    output_tuples = [ (output[key], "{}/{}_{}.csv".format(output_dir,key,year)) for key in output ]
    for file in output_tuples:
        file[0].to_csv(file[1], index=False, sep="|")

# All types of weirdness in the underlying text files. Using type casting to ensure upload to database.
expenditure_dtypes = {
    'doc_seq_no': 'int64',
     'expenditure_type': 'object',
     'gub_account_type': 'object',
     'gub_elec_type': 'object',
     'page_no': 'int64',
     'expense_id': 'int64',
     'detail_id': 'int64',
     'doc_stmnt_year': 'int64',
     'doc_type_desc': 'object',
     'com_legal_name': 'object',
     'common_name': 'object',
     'cfr_com_id': 'int64',
     'com_type': 'object',
     'schedule_desc': 'object',
     'exp_desc': 'object',
     'purpose': 'object',
     'extra_desc': 'object',
     'f_name': 'object',
     'lname_or_org': 'object',
     'address': 'object',
     'city': 'object',
     'state': 'object',
     'zip': 'object',
     'exp_date': 'object',
     'amount': 'object',
     'state_loc': 'float64',
     'supp_opp': 'float64',
     'can_or_ballot': 'object',
     'county': 'object',
     'debt_payment': 'object',
     'vend_name': 'object',
     'vend_addr': 'object',
     'vend_city': 'object',
     'vend_state': 'object',
     'vend_zip': 'object',
     'gotv_ink_ind': 'object',
     'fundraiser': 'object'
}

contribution_dtypes = {
    'doc_seq_no': 'float64',
     'page_no': 'float64',
     'contribution_id': 'float64',
     'cont_detail_id': 'float64',
     'doc_stmnt_year': 'float64',
     'doc_type_desc': 'object',
     'com_legal_name':'object',
     'common_name': 'object',
     'cfr_com_id': 'float64',
     'com_type': 'object',
     'can_first_name': 'object',
     'can_last_name':'object',
     'contribtype':'object',
     'f_name':'object',
     'l_name_or_org':'object',
     'address': 'object',
     'city':'object',
     'state': 'object',
     'zip': 'object',
     'occupation': 'object',
     'employer': 'object',
     'received_date': 'object',
     'amount': 'object',
     'aggregate': 'float64',
     'extra_desc': 'object'
}

receipt_dtypes = {
    'doc_seq_no': 'int64',
     'ik_code': 'object',
     'gub_account_type': 'object',
     'gub_elec_type': 'object',
     'page_no': 'object',
     'receipt_id': 'object',
     'detail_id': 'object',
     'doc_stmnt_year': 'object',
     'doc_type_desc': 'object',
     'com_legal_name': 'object',
     'common_name': 'object',
     'cfr_com_id': 'int64',
     'com_type': 'object',
     'can_first_name': 'object',
     'can_last_name': 'object',
     'contribtype': 'object',
     'f_name': 'object',
     'l_name_or_org': 'object',
     'address': 'object',
     'city': 'object',
     'state': 'object',
     'zip': 'object',
     'occupation': 'object',
     'employer': 'object',
     'received_date': 'object',
     'amount': 'object',
     'aggregate': 'float64',
     'extra_desc': 'object',
     'receipttype': 'object'
}

def load_campaign_finance_csv(csv_loc, table):
    pwd = os.environ["SHARED_PASSWORD"]
    engine = create_engine(
        "postgres://shared:{pwd}@postgres/shared".format(**locals()))
    if 'expenditures' in csv_loc:
        chunks = pd.read_csv(csv_loc, sep="|", dtype=expenditure_dtypes, chunksize=500000)        
    if 'receipts' in csv_loc:
        chunks = pd.read_csv(csv_loc, sep="|", dtype=receipt_dtypes, chunksize=500000)
    if 'contributions' in csv_loc:
        chunks = pd.read_csv(csv_loc, sep="|", dtype=contribution_dtypes, chunksize=500000)
    for chunk in chunks: 
        chunk.to_sql(table, engine, if_exists='append', index=False)