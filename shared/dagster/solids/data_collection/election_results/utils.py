import os

import pandas as pd
from sqlalchemy import create_engine

from ..web_requests import get_zip_file

county_columns = [
    'county_code',
    'county_name'
]

office_columns = [
    'election_year',
    'election_type',
    'office_code',
    'district_code',
    'status_code',
    'office_description'
]

vote_columns = [
    'election_year',
    'election_type',
    'office_code',
    'district_code',
    'status_code',
    'candidate_id',
    'county_code',
    'city_code',
    'ward_number',
    'precinct_number',
    'precinct_label',
    'precinct_votes'
]

name_columns = [
    'election_year',
    'election_type',
    'office_code',
    'district_code',
    'status_code',
    'candidate_id',
    'candidate_last_name',
    'candidate_first_name',
    'candidate_middle_name',
    'candidate_party_name'
]

city_columns = [
    'election_year',
    'election_type',
    'county_code',
    'city_code',
    'city_description'
]

def read_election_url(url, sep="\t", header=True):
    zipfile = get_zip_file(url)
    output = {
        'county': None,
        'vote': None,
        'offices': None,
        'name': None,
        'city': None
    }
    for name in zipfile.namelist():
        if 'readme' not in name:
            df = pd.read_csv(zipfile.open(name), sep=sep, header=header)
            df = df.loc[:, list(df.columns.values)[:-1]]
        if 'county' in name:
            df.columns = county_columns
            df = df.loc[:, df.columns]
            output['county'] = df
        if 'offc' in name:
            df.columns = office_columns
            df = df.loc[:, df.columns]
            output['offices'] = df
        if 'vote' in name:
            df.columns = vote_columns
            df = df.loc[:, df.columns]
            output['vote'] = df
        if 'name' in name:
            df.columns = name_columns
            df = df.loc[:, df.columns]
            output['name'] = df
        if 'city' in name:
            df.columns = city_columns
            df = df.loc[:, df.columns]
            output['city'] = df
    return output


def read_year(year):
    url = f'http://miboecfr.nictusa.com/cfr/presults/{year}GEN.zip'
    data = read_election_url(url, header=None)
    return data


def read_election_results(year):
    output = {
        'county': None,
        'vote': None,
        'offices': None,
        'name': None,
        'city': None
    }
    data = read_year(year)
    for key in output:
        output[key] = data[key]
    return output

election_data_tables = [
    'vote',
    'offices',
    'name',
    'county',
    'city'
]

yearly_tables = [
    'vote',
    'offices',
    'name'
]

def delete_tables():
    pwd = os.environ["SHARED_PASSWORD"]
    engine = create_engine(
        "postgres://shared:{pwd}@postgres/shared".format(**locals()))
    raw_query = "DROP TABLE {};"
    for table in election_data_tables:
        query = raw_query.format(table)
        engine.execute(query)

def check_for_tables(engine):
    missing_tables = []
    for table in election_data_tables:
        if not engine.dialect.has_table(engine, table):
            missing_tables.append(table)
    return missing_tables


def missing_table_years(years, engine, table):
    query = "SELECT election_year from {} GROUP BY election_year".format(table)
    df = pd.read_sql(query, engine)
    table_years = list(df['election_year'].unique())
    table_years = [ str(year) for year in table_years ]
    missing_years = []
    for year in years:
        if year not in table_years:
            missing_years.append(year)
    return missing_years

    
def check_election_data(years):
    pwd = os.environ["SHARED_PASSWORD"]
    engine = create_engine(
        "postgres://shared:{pwd}@postgres/shared".format(**locals()))
    needed_files = { year: [] for year in years }
    missing_tables = check_for_tables(engine)
    for year in years:
        for table in missing_tables:
            needed_files[year].append(table)
    for table in yearly_tables:
        if table not in missing_tables:
            missing_years = missing_table_years(years, engine, table)
            for year in missing_years:
                needed_files[year].append(table)
    return needed_files

def load_election_csv(csv_loc, table):
    pwd = os.environ["SHARED_PASSWORD"]
    engine = create_engine(
        "postgres://shared:{pwd}@postgres/shared".format(**locals()))
    chunks = pd.read_csv(csv_loc, sep="|", chunksize=50000)
    for chunk in chunks:
        chunk.to_sql(table, engine, if_exists='append', index=False)