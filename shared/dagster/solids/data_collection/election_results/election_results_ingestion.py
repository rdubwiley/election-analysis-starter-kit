import os
from zipfile import ZipFile

from dagster import (
    composite_solid,
    solid, 
    Field, 
    Bool,
    String,
    Optional,
    Dict,
    List,
    as_dagster_type,
    Output,
    OutputDefinition,
    Materialization,
    EventMetadataEntry,
    JsonMetadataEntryData
)

from .utils import (
    delete_tables,
    check_election_data,
    read_election_results,
    load_election_csv,
)

@solid()
def drop_tables(context):
    delete_tables()
    return True

@solid()
def check_years(context, years:List[String], check:Optional[Bool]=None) -> Dict:
    check = check_election_data(years)
    for year in check:
        if not check[year]:
            context.log.info(f"All good for year {year}")
    return check

@solid()
def save_election_data(context, year_dict:Dict) -> List[Dict]:
    output_files = []
    for year in year_dict:
        if year_dict[year]:
            data = read_election_results(year)
            for table in year_dict[year]:
                filename = f'./data/election_results/{table}_{year}.csv'
                data[table].to_csv(filename, index=False, sep="|")
                output_files.append({
                    'location': filename,
                    'table': table
                })
    return output_files

@solid()
def upload_election_data(context, data_files:List[Dict]) -> List[Dict]:
    if not os.path.isdir("data"):
        os.makedirs("./data")
    if not os.path.isdir("data/election_results"):
        os.makedirs("./data/election_results")
    for item in data_files:
        load_election_csv(item['location'], item['table'])
        context.log.info(f"Loaded file {item['location']}")
    return data_files

@solid()
def cleanup_data(context, data_files:List[Dict]):
    for data in data_files:
        context.log.info(f"Removed {data['location']}")
        os.remove(data['location'])
