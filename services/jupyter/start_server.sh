export J_PASS="$(python -c 'from notebook.auth import passwd; import os; print(passwd(os.environ["PASSWORD"]).strip())')"
echo "c.NotebookApp.password='$J_PASS'">>/root/.jupyter/jupyter_notebook_config.py
jupyter notebook --notebook-dir=/tf --ip 0.0.0.0 --no-browser --allow-root
