#!/bin/bash
set -e # exit if a command exits with a not-zero exit code

POSTGRES="psql -U postgres"

# create a role for airflow
echo "Creating database role: api"
$POSTGRES <<-EOSQL
CREATE USER api WITH
    LOGIN
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    NOINHERIT
    NOREPLICATION
    PASSWORD '$API_PASSWORD';
EOSQL

