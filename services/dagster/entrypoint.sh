#!/bin/sh

# see: https://unix.stackexchange.com/a/453053 - fix link-count
touch /etc/crontab /etc/cron.*/*

service cron start

export DAGSTER_HOME=/dagster

# Add all schedules
/usr/local/bin/dagster schedule up

# Restart previously running schedules
/usr/local/bin/dagster schedule restart --restart-all-running

python3 -m dagit -h 0.0.0.0